# stack_max

Simple mod to modify the default item stack maximum. Includes a menu setting to set it and a function (`stack_max.set_default`) for mods and games to override it. The default value is `100`.

Credit to prestidigitator (forum username) for the code snippet this mod is based on.