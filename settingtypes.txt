# Items that do not provide an explicit stack maximum value
# will be modified to use this value instead.
# 
# Good values include 100 (easy mental math), 72 (divisible by
# a lot of numbers), and 144.
stack_max.default (Default item stack maximum) int 100 1 65535

# If enabled, items that explicitly define a stack maximum
# of 99 will also be overridden with the new default value.
stack_max.fix_99 (Fix certain items that have a max of 99) bool true