-- Modified version of a code snippet by prestidigitator (forum username).
-- Yay™

-- Usage:
-- * `stack_max.set_default(new_value)`: Sets the new default item stack maximum. Applies to all items that have been registered previously, and all items to be registered in the future.

stack_max = {}
local DEFAULT_STACK_MAX = tonumber(minetest.setting_get("stack_max.default") or 100)
local FIX_99_STACK_MAX = minetest.setting_get("stack_max.fix_99")

local function wrapRegFunc(funcName)
	local wrapped = minetest[funcName]
	minetest[funcName] = function (name, def, ...)
		if def.stack_max == nil or (FIX_99_STACK_MAX and def.stack_max == 99) then def.stack_max = DEFAULT_STACK_MAX; def._default_stack_max = true end
		return wrapped(name, def, ...)
	end
end
wrapRegFunc('register_node')
wrapRegFunc('register_craftitem')
wrapRegFunc('register_item')
wrapRegFunc('register_tool')

local function resetOldDefinitions()
	for name, def in pairs(minetest.registered_items) do
		if def.stack_max == nil or (FIX_99_STACK_MAX and def.stack_max == 99) or def._default_stack_max then
			minetest.override_item(name, {
				stack_max = DEFAULT_STACK_MAX
			})
		end
	end
end
resetOldDefinitions()

stack_max.set_default = function (new_value)
	DEFAULT_STACK_MAX = new_value
	resetOldDefinitions()
end